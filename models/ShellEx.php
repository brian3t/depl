<?php

namespace app\models;

class ShellEx
{
  public int $retval = -8;
  public string $message = '';
  const PRE_COMMAND = 'cd /var/www/sfran';

  function __construct() {
    $shell_output = null;
    $retval = null;
    exec('cd /var/www/sfran', $output, $retval);
    if ($retval != 0) {
      $this->retval = -1;
      $this->message = "Error during construction `cd` ";
      return false;
    }

    $this->retval = 1;
    $this->message = 'ShellEx Initialized';
    return true;
  }

  public function get_current_info(): array {
    $git_status = '';
    $res = ['branch' => '', 'git_status' => $git_status];
    $branch = 'unknown';
    $output = [];
    $ex_retval = null;
    $this::exe(" git status "
    ,$mg_gitstat_msg, $mg_gitstat_retval);
    if ($mg_gitstat_retval != 1) return ['git_status' => $mg_gitstat_msg];
    $res['git_status'] = $mg_gitstat_msg;
    $this::exe('git remote -v', $output, $ex_retval);
    if ($ex_retval != 1) return $res;
    $res['git_remote_url'] = $output;
    $res['branch'] = $branch;
    return $res;
  }

  /**
   * Check out git branch
   * @param $branch
   * @return array Retcode and Message
   */
  public function checkout($branch): array {
    $retval = 8;
    $msg = '';
    $ex_output = null;
    $ex_retval = null;
    $this::exe("git stash && git fetch && git checkout $branch && git pull && git status", $ex_output, $ex_retval);
    $res = [$ex_retval, $ex_output];
    return $res;
  }

  /**
   * Run git pull
   * @return array Retcode and Message
   */
  public function git_pull(): array {
    $retval = 8;
    $msg = '';
    $ex_output = null;
    $ex_retval = null;
    $this::exe("git pull && git status", $ex_output, $ex_retval);
    $res = [$ex_retval, $ex_output];
    return $res;
  }

  /**
   * Wrapper of exec(). Append `cd /var/www` before any execution
   * @param $command
   * @param $output
   * @param $retval
   * @return array
   */
  public function exe($command, &$output, &$retval): array {
//    $command = "git remote -v && " . $command . " 2>&1";
    $command = self::PRE_COMMAND . " && " . $command . " 2>&1";
    $retval = 8;
//    chdir("/var/www/sfran");
//    exec($command, $output, $retval);
    $output = shell_exec($command);
    $retval = 1;
    return [$retval, $output];
  }


}
