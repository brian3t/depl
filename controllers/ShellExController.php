<?php

namespace app\controllers;

use app\models\ShellEx;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;

class ShellExController extends Controller
{
  /*public function behaviors(): array {
    $behaviors = parent::behaviors();
    $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;

    return ArrayHelper::merge([
/*      [
        'class' => Cors::className(),
        'cors' => [
          'Origin' => ['*'],
          'Access-Control-Request-Methods' => ['GET', 'POST', 'OPTIONS', 'DELETE', 'PUT', 'PATCH'],
        ],
      ],
      // 'authenticator' => ['class' => HttpBasicAuth::className()]
    ], $behaviors);
  }*/

  public function actionCheckout(): string {
    $branch = \Yii::$app->request->get('branch');
    if (empty($branch)) return json_encode([-1, 'Must provide param branch']);
    $shell_ex = new ShellEx();
    [$checkout_retval, $checkout_msg] = $shell_ex->checkout($branch);
    return json_encode([$checkout_retval, $checkout_msg]);
  }

  public function actionGitStatus(): string {
    $shell_ex = new ShellEx();
    $shell_ex->exe(" git status "
    ,$mg_gitstat_msg, $mg_gitstat_retval);
    return json_encode([$mg_gitstat_retval, $mg_gitstat_msg]);
  }

  public function actionGitPull(): string {
    $shell_ex = new ShellEx();
    [$checkout_retval, $checkout_msg] = $shell_ex->git_pull();
    return json_encode([$checkout_retval, $checkout_msg]);
  }

  public function actionMagentoClean(): string {
    $shell_ex = new ShellEx();
    $shell_ex->exe("disxdebug ; ./bin/magento s:upg ; rm -rf ./var/view_preprocessed ; rm -rf ./pub/static/adminhtml ; rm -rf ./pub/static/frontend ; bin/magento cache:clean ; bin/magento cache:flush ; bin/magento indexer:reindex ; bin/magento s:d:c"
    ,$mg_clean_msg, $mg_clean_retval);
    return json_encode([$mg_clean_retval, $mg_clean_msg]);
  }

  public function actionTouchFile(): string {
    $shell_ex = new ShellEx();
    $shell_ex->exe(" touch asdf.txt "
    ,$mg_clean_msg, $mg_clean_retval);
    return json_encode([$mg_clean_retval, $mg_clean_msg]);
  }

  public function actionIndex(): string {
    $branch = \Yii::$app->request->get('branch');

    return json_encode([1, 'index hello']);
  }
}
