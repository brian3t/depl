$(function () {
  axios.interceptors.request.use((config) => {
    $('#spinner').show()
    return config;
  });

  function hide_loading() {
    $('#spinner').hide()
  }

  $('#checkout_branch_btn').on('click', async function () {
    console.log(`checkout branch clicked`)
    const branch = $('#branch_to_check').val()
    let checkout_res = await axios.get(`/shell-ex/checkout?branch=${branch}`)
    hide_loading()
    if (checkout_res.status != 200) { //asdf
      return app_flash(`Error: ` + checkout_res.statusText)
    }
    checkout_res = checkout_res?.data || []
    let [retcode, msg] = checkout_res
    if (msg instanceof Array) msg = msg.join(' ')
    if (retcode == 1) msg += `. Checked out successfully. Refresh this page to see updated current branch`
    app_flash(msg)
    $('#exe_output').text($('#exe_output').text() + ' ' + msg)
  })

  $('#magento_clean_btn').on('click', async function () {
    console.log(`magento_clean_btn clicked`)
    let checkout_res = await axios.get(`/shell-ex/magento-clean`)
    hide_loading()
    if (checkout_res.status != 200) { //asdf
      return app_flash(`Error: ` + checkout_res.statusText)
    }
    checkout_res = checkout_res?.data || []
    let [retcode, msg] = checkout_res
    app_flash(msg)
    $('#exe_output').text(msg.join('  '))
  })

  $('#git_pull_btn').on('click', async function () {
    let git_pull_res = await axios.get(`/shell-ex/git-pull`)
    hide_loading()
    if (git_pull_res.status != 200) { //asdf
      return app_flash(`Error: ` + git_pull_res.statusText)
    }
    git_pull_res = git_pull_res?.data || []
    let [retcode, msg] = git_pull_res
    app_flash(msg)
    $('#exe_output').text(msg)
  })
})
