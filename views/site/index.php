<?php

/** @var yii\web\View $this */
/** @var \app\models\ShellEx $model */

$this->title = 'Deployment tool SocalAppSolutions';
$current_info = $model->get_current_info();
$branch = $current_info['branch'] ?? 'unknown';
$git_status = $current_info['git_status'] ?? 'unknown';
$git_remote_url = $current_info['git_remote_url'] ?? 'unknown';
try {
  $this->registerJsFile(
    '@web/js/index.js',
    ['depends' => [\yii\web\JqueryAsset::class]]
  );
} catch (\yii\base\InvalidConfigException $e) {
}
?>
<div class="site-index">
    <div class="body-content">
        <div id="spinner" class="spinner-border " role="status" style="display: none">
            <span class="visually-hidden">Loading...</span>
        </div>
        <h4>Deployment tool SocalAppSolutions</h4>
    </div>
  <h5>Remote url: <?= $git_remote_url ?></h5>
    <div id="current_info">
        <!--        Current branch: <span>--><?php //= $branch ?><!--</span>-->
        <br>
        <hr>
        Git status: <?= $git_status ?>
    </div>
    <br>
    <hr>
    <label for="branch_to_check">Branch to Checkout</label>
    <input type="text" id="branch_to_check" name="branch_to_check"/>
    <button id="checkout_branch_btn" class="btn btn-primary">Checkout Branch</button>
    <br><br>
    <button id="git_pull_btn" class="btn btn-primary">Run Git Pull</button>
    <br><br>
    <button id="magento_clean_btn" class="btn btn-primary">Run Magento Compile Command</button>
    <br><br>
    <label for="exe_output">Execution Output</label><br>
    <textarea name="exe_output" id="exe_output" cols="80" rows="10" readonly disabled>

    </textarea>
</div>
